from hdfs import InsecureClient
from pathlib import Path

# https://hdfscli.readthedocs.io/en/latest/quickstart.html

client = InsecureClient('http://namenode:50070/')

client.makedirs('/user/hive/warehouse/temp/', permission=None)

client.makedirs('/user/hive/warehouse/test_db/csv/', permission=None)

fnames = client.list('/user/hive/warehouse/test_db/csv/')
print(fnames)

fnames = client.list('/user/hive/warehouse/test_db/')
print(fnames)

#for filename in Path('/tmp/geo-dvf/').rglob('*.csv'): # Marche pas en local pour d'obscures raisons liés au module python hdfs, doit être testé directement dans l'infra
for filename in Path('/csv_data_directory/geo-dvf/').rglob('*.csv'):
    print(filename)
    client.upload('/user/hive/warehouse/test_db/csv/', filename, temp_dir='/user/hive/warehouse/temp/', overwrite=True)
