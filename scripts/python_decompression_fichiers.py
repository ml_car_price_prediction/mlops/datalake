from sh import gunzip
from pathlib import Path

for filename in Path('/tmp/geo-dvf/').rglob('*.csv.gz'):
    print(filename)
    gunzip(filename)