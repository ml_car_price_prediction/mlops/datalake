import requests
import re
from pathlib import Path

pattern = re.compile(r'"(\d{4})/"')
result = requests.get("https://files.data.gouv.fr/geo-dvf/latest/csv/").text
print(result)

Path('/tmp/geo-dvf/').mkdir(parents=True, exist_ok=True)
for (annee) in re.findall(pattern, result):
    print(annee)
    r = requests.get(f'https://files.data.gouv.fr/geo-dvf/latest/csv/{annee}/full.csv.gz')
    with open(f'/tmp/geo-dvf/geo-dvf-{annee}.csv.gz','wb') as f:
        f.write(r.content)
