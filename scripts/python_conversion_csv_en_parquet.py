import requests
import re
from pathlib import Path
import subprocess

for filename in Path('/csv_data_directory/geo-dvf/').rglob('*.csv'):
    print(filename)
    
    parquet_filename = str(filename).replace('.csv', '.parquet')
    bashCommand = f"/csv_data_directory/csv2parquet --max-read-records=0 --header=true --compression=snappy {filename} {parquet_filename}"
    process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
    output, error = process.communicate()

    print(output)
    print(error)