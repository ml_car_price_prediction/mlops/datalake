FROM python:3.9.16-slim-bullseye

WORKDIR /app

COPY test.py ./

CMD [ "python", "./test.py"]